console.log("JS is connected");

(function() {

var angularModule = angular.module('mainApp', ['ui.router']);

// configuring our routes 
// =============================================================================
angularModule.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('dashboard', {
            url: '/Dashboard',
            templateUrl: 'views/dashboard.html',
            controller: 'mainController'
        })
        .state('dp', {
            url: '/dp',
            templateUrl: 'views/dp.html',
            controller: 'mainController'
        })
        .state('dashboard.results', {
            url: '/results',
            templateUrl: 'views/dash-results.html',
            controller: 'mainController'
        })
        .state('dashboard.searchResults', {
            url: '/searchResults',
            templateUrl: 'views/search-results.html'
        })
        .state('dashboard.tileResults', {
            url: '/tileResults',
            templateUrl: 'views/results-tiles.html'
        })
        .state('dashboard.dp-model', {
            url: '/dp-model',
            templateUrl: 'views/dp-model.html'
        })
        .state('successStories', {
            url: '/sucessStories',
            templateUrl: 'views/successStories.html'
        })
        .state('dashboard.successStoriesModel', {
            url: '/sucessStoriesModel',
            templateUrl: 'views/successStoriesModel.html'
        })
        .state('dashboard.details', {
            url: '/details',
            templateUrl: 'views/details.html'
        })
        .state('start', {
            url: '/start',
            templateUrl: 'views/start.html',
            controller: 'mainController'
        });
        
    $urlRouterProvider.otherwise('/Dashboard');
}]);

angularModule.filter('highlight', function($sce) {
    return function(text, phrase) {
      if (phrase) text = text.replace(new RegExp('('+phrase+')', 'gi'),
        '<span class="highlighted">$1</span>');

      return $sce.trustAsHtml(text);
    };
  });
// our controller
// =============================================================================
angularModule.controller('mainController', ['$scope','$http','$state',function( $scope,$http,$state) {
$scope.searchInput='';
 $.getJSON('data/keywords.json', function(data) {
        $scope.inputSearchList= data;
  });
//$scope.inputSearchList =[
//      "ActionScript",
//      "AppleScript",
//      "Asp",
//      "BASIC",
//      "C",
//      "C++",
//      "Clojure",
//      "COBOL",
//      "ColdFusion",
//      "Erlang",
//      "Fortran",
//      "Groovy",
//      "Haskell",
//      "Java",
//      "JavaScript",
//      "Lisp",
//      "Perl",
//      "PHP",
//      "Python",
//      "Ruby",
//      "Scala",
//      "Scheme"
//    ];
(function(){
   $http({
        method : "GET",
        url : "http://ch3lxcmwasd01.corp.equinix.com:9200/"
        }).then(function mySucces(response) {
            console.log("Elastic search: Conected");
        }, function myError(response) {
            console.log("error:"+response.statusText); 
            alert("Your are not connected to elastic search: Check VPN");
        }); 
})();
$scope.scrollingIt = function(){
  console.log("scrolling This dude");
};
 $scope.properties = {};
  $scope.ss = {};
  $scope.dp={};
  if(sessionStorage.getItem('ssLocal') != '' || typeof sessionStorage.getItem('ssLocal') != 'undefined' )
{
    $scope.ss = JSON.parse(sessionStorage.getItem('ssLocal'));
}
  if(sessionStorage.getItem('dpLocal') != '' || typeof sessionStorage.getItem('dpLocal') != 'undefined' )
{
    $scope.dp = JSON.parse(sessionStorage.getItem('dpLocal'));
}
$scope.elasticURL = "";
$scope.bluePrints =[];
$scope.getSSDetails = function(id){
     $http({
        method : "GET",
        url : "http://ch3lxcmwasd01.corp.equinix.com:9200/kb/ss/_search?q=_id:"+id
        }).then(function mySucces(response) {
           console.log("ss-details:ID-"+id);
           //console.log("Full Obj:"+JSON.stringify(response.data));
           $scope.ss = {};
           $scope.ss["id"] = response.data.hits.hits[0]['_id'];
           $scope.ss["industry_vertical"] = response.data.hits.hits[0]['_source']['industry_vertical'];
           $scope.ss["region"] = response.data.hits.hits[0]['_source']['region'];
           $scope.ss["customer"] = response.data.hits.hits[0]['_source']['customer'];
           $scope.ss["primary_plcd"] = response.data.hits.hits[0]['_source']['primary_plcd'];
           $scope.ss["secondary_plcd"] = response.data.hits.hits[0]['_source']['secondary_plcd'];
           $scope.ss["business_summary"] = response.data.hits.hits[0]['_source']['business_summary'];
           $scope.ss["deployment_pattern_id"] = response.data.hits.hits[0]['_source']['deployment_pattern_id'];
           $scope.ss["author"] = response.data.hits.hits[0]['_source']['author'];
          // $scope.ss["requirements"]= [response.data.hits.hits[0]['_source']['customer_requirement_#1'],response.data.hits.hits[0]['_source']['customer_requirement_#2'],response.data.hits.hits[0]['_source']['customer_requirement_#3'],response.data.hits.hits[0]['_source']['customer_requirement_#4']];
           $scope.ss.requirements = [];
           $scope.ss.locations = [];
           for(var i=1; i<14; i++)
           {
            //console.log("Location:i="+i+"-"+ response.data.hits.hits[0]['_source']['location_#'+i]);
            if(response.data.hits.hits[0]['_source']['location_#'+i] != '')   
                $scope.ss["locations"][$scope.ss["locations"].length] = response.data.hits.hits[0]['_source']['location_#'+i];  
           }  
           for(var i=1; i<9; i++)
           {
            if(response.data.hits.hits[0]['_source']['customer_requirement_#'+i] != '')   
                $scope.ss["requirements"][$scope.ss["requirements"].length] = response.data.hits.hits[0]['_source']['customer_requirement_#'+i]; 
           }  
           $scope.ss["productsSold"] = [];
           for(var i=1; i<7; i++)
           {
              if(response.data.hits.hits[0]['_source']['products_sold_#'+i] != '') 
              {
                  //console.log("p:"+i +" -"+response.data.hits.hits[0]['_source']['products_sold_#'+i]);
                  $scope.ss["productsSold"][$scope.ss["productsSold"].length] = response.data.hits.hits[0]['_source']['products_sold_#'+i];
              }
           } 
             
           $scope.ss["results"] = [];
           for(var i=1; i<8; i++)
           {
              if(response.data.hits.hits[0]['_source']['benefit_/_result_#'+i] != '') 
              {
                 // console.log("r:"+i +" -"+response.data.hits.hits[0]['_source']['benefit_/_result_#'+i]);
                  $scope.ss["results"][$scope.ss["results"].length] = response.data.hits.hits[0]['_source']['benefit_/_result_#'+i];
              }
           }
           $scope.ss["soundBytes"]=[];
           $scope.ss["soundBytes"][0]= response.data.hits.hits[0]['_source']['solution_overview_/_sales_soundbite'];
           
           sessionStorage.setItem("ssLocal", JSON.stringify($scope.ss));
           //console.log("sessionObj:"+ sessionStorage.getItem('ssLocal'));
          // console.log("looking dp id:"+$scope.ss["deployment_pattern_id"]);
           $scope.getDpDetails($scope.ss["deployment_pattern_id"]);
           
          // console.log("response:"+JSON.stringify(response.data.hits.hits[0]['_source']['industry_vertical']));
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        });  
       
      // window.open("","_self");
};
$scope.getSSDetailsNT = function(id){
    console.log("SS ID"+id);
    $scope.getSSDetails(id);
    setTimeout(function(){ 
           window.open('/IOA_KnowledgeBase-2/index.html#/sucessStories', '_blank'); }, 200);
};
$scope.getDpDetails = function(id){
    console.log("DP ID:"+id);
     $http({
        method : "GET",
        url : "http://ch3lxcmwasd01.corp.equinix.com:9200/kb/dp/_search?q=_id:"+id
        }).then(function mySucces(response) {
           //console.log("beforeImage:"+ response.data.hits.hits[0]._source.before_image);
           $scope.dp={};
           $scope.dp.id = response.data.hits.hits[0]._id;
           $scope.dp.name = response.data.hits.hits[0]._source.name;
           $scope.dp.summary = response.data.hits.hits[0]._source.description;
           $scope.dp.ioa_use_case = response.data.hits.hits[0]._source.ioa_use_case;
           $scope.dp.before_image = response.data.hits.hits[0]._source.before_image;
           $scope.dp.after_image = response.data.hits.hits[0]._source.after_image;
           $scope.dp.challenges = [];
           $scope.dp.benefits = [];
           for(var i=1; i<6; i++)
           {
              if(response.data.hits.hits[0]._source['challenge_#'+i] != '') 
              {
                 // console.log("r:"+i +" -"+response.data.hits.hits[0]['_source']['benefit_/_result_#'+i]);
                 $scope.dp.challenges[$scope.dp.challenges.length] = response.data.hits.hits[0]._source['challenge_#'+i];
              }
              if(response.data.hits.hits[0]._source['benefit_#'+i] != '') 
              {
                 $scope.dp.benefits[$scope.dp.benefits.length] = response.data.hits.hits[0]._source['benefit_#'+i];
              }
           }
           sessionStorage.setItem("dpLocal", JSON.stringify($scope.dp));;
           //console.log("response:"+response.data);
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        });  
};
$scope.getDpDetailsNT = function(id){
    $scope.getDpDetails(id);
    setTimeout(function(){ window.open('/IOA_KnowledgeBase-2/index.html#/dp', '_blank'); }, 200);
};
$scope.checkEmpty = function(dataIn){
    if(dataIn === '')
        return true;
    return false;
};
 $.getJSON('data/properties.json', function(data) {
         $scope.properties = data;
        // console.log("updated properties:"+JSON.stringify($scope.properties.elasticURL));
         $scope.elasticURL = $scope.properties.elasticURL;
         //console.log("elasticURL:"+$scope.elasticURL);
  });
    $scope.detailsObject = {};
    $scope.changeDetailsObj = function(){
        $scope.detailsObject.title = "Title has been chagned";
        $scope.detailsObject.description = "Congratulations description  has also been update now you can make any changes you want";
    };
    $scope.getFileter = function(){
        $.getJSON('data/filters.json', function(data) {
        for(var i=0; i< $scope.sideMenuObjs.length ; i++)
            {  $scope.sideMenuObjs[i].options = [];
               data.aggregations[$scope.sideMenuObjs[i].id].buckets;
               for(var j=0 ; j<data.aggregations[$scope.sideMenuObjs[i].id].buckets.length ; j++)
               {
                   $scope.sideMenuObjs[i].options.push(data.aggregations[$scope.sideMenuObjs[i].id].buckets[j].key);
                   //console.log("Filters:"+JSON.stringify(data.aggregations[$scope.sideMenuObjs[i].id].buckets[j].key));
               }
            }  
        //console.log("getFilter() side Menu:"+JSON.stringify($scope.sideMenuObjs));
        $scope.selectAll();
        }); 
      
    };
    $scope.filtersActive = [
        {
            "id":"industry_vertical",
            "category":"Industry Vertical",
            "options":[]
        },
        {   "id":"industry_1",
            "category":"industry1",
            "options":[]
        },
        {   "id":"ioa_use_case",
            "category":"Ioa use case",
            "options":[]
        },
        {
            "id":"region",
            "category":"Region",
            "options":[]
        }
    ];
    $scope.resultList = [];
    $scope.filtersOnResults = [];
$scope.ssResultList=[];    
$scope.getSuccessStories = function(){
       //console.log("getDP function");
        var queryString ="http://ch3lxcmwasd01.corp.equinix.com:9200/kb/ss/_search?fields=_id,customer,business_summary&q=";
        var filtersObj = [];
        //console.log("filtersActive:"+JSON.stringify($scope.filtersActive));
        for(var i =0 ; i<$scope.filtersActive.length ;i++)
        {
           if($scope.filtersActive[i].options.length !== 0){
                filtersObj.push($scope.filtersActive[i]);
            }
        }
        
       $scope.ssResultList=[];
      //  console.log("PLCD_Active:"+JSON.stringify(filtersObj));
            queryString = queryString + "(";
            for(var i =0 ; i<filtersObj.length ;i++)
            {
             //   console.log("filetersObj.options.length:"+filtersObj[i].options.length);
                queryString = queryString + "(";
                for(var j=0; j< filtersObj[i].options.length; j++)
                 {

                      if(j === 0)
                      {  
                        if(filtersObj[i].id === "ioa_use_case" )
                        queryString = queryString +"primary_plcd"+":\""+ filtersObj[i].options[j].replace("&","%26")+"\"";
                        else
                        queryString = queryString +filtersObj[i].id+":\""+ filtersObj[i].options[j].replace("&","%26")+"\"";
                      }
                    else
                    {
                        if(filtersObj[i].id === "ioa_use_case" )
                        queryString = queryString +" OR "+"primary_plcd"+":\""+ filtersObj[i].options[j].replace("&","%26")+"\"";
                        else
                        queryString = queryString +" OR "+filtersObj[i].id+":\""+ filtersObj[i].options[j].replace("&","%26")+"\"";
                    } 
               //     console.log("obj:"+filtersObj[i].options[j]) ;
                 }
                 queryString = queryString+")";
                if(i < filtersObj.length-1 )
                {
                    queryString = queryString +" AND ";
                }      
            }
             queryString = queryString+")";
            if(i < filtersObj.length-1 )
            {
                queryString = queryString +" AND ";
            }      
        $http({
        method : "GET",
        //url : "http://localhost:9200/companies/company/_search?fields=_id,customer,customer_description&q=industry_vertical:(%22Manufacturing%22%20OR%20%22Health%20Care%22)%20AND%20primary_plcd:%22Locations%22"
        url : queryString+"&size=9999"
        }).then(function mySucces(response) {
          // console.log("response:"+JSON.stringify(response.data) );//$scope.ssResultList=[];
           for(var i=0; i<response.data.hits.hits.length; i++){
               var ssLi = {
                   "id":"",
                   "name":"",
                   "business_summary":""
               };
               ssLi.name = response.data.hits.hits[i].fields.customer[0];
               ssLi.id = response.data.hits.hits[i]._id;
               //ssLi.business_summary = response.data.hits.hits[i].business_summary;
               ssLi.business_summary = response.data.hits.hits[i].fields.business_summary[0];
               $scope.ssResultList.push(ssLi);
           }
      //  console.log("ssLi:"+ $scope.ssResultList.length); 
        }, function myError(response) {
            console.log("error:"+response.statusText); 
           $scope.ssResultList=[];
        });  
        //console.log("ss-query:"+queryString); 
    };
$scope.dpresultList=[];
$scope.getdbs = function(){
      // console.log("getDBs function");
        var queryString ="http://ch3lxcmwasd01.corp.equinix.com:9200/kb/dp/_search?fields=_id,name,description&q=";
        var filtersObj = [];
      //  console.log("filtersActive:"+JSON.stringify($scope.filtersActive));
        for(var i =0 ; i<$scope.filtersActive.length ;i++)
        {
            if($scope.filtersActive[i].id === "ioa_use_case"){
                filtersObj.push($scope.filtersActive[i]);
            }
        }
       // console.log("PLCD_Active:"+JSON.stringify(filtersObj));
            queryString = queryString + "(";
            for(var j=0; j< filtersObj[0].options.length; j++)
             {
                 if(j === 0)
                    queryString = queryString +filtersObj[0].id+":\""+ filtersObj[0].options[j].replace("&","%26")+"\"";
                  else
                    queryString = queryString +" OR "+filtersObj[0].id+":\""+ filtersObj[0].options[j].replace("&","%26")+"\"";
               // console.log("obj:"+filtersObj[0].options[j]) ;
             }
             queryString = queryString+")";
            if(i < filtersObj.length-1 )
            {
                queryString = queryString +" AND ";
            }      
        $http({
        method : "GET",
        //url : "http://localhost:9200/companies/company/_search?fields=_id,customer,customer_description&q=industry_vertical:(%22Manufacturing%22%20OR%20%22Health%20Care%22)%20AND%20primary_plcd:%22Locations%22"
        url : queryString+"&size=9999"
        }).then(function mySucces(response) {
         //  console.log("response:"+response.data);
            $scope.dpresultList=[];
           for(var i=0; i<response.data.hits.hits.length; i++){
               var dpLi = {
                   "id":"",
                   "name":"",
                   "description":""
               };
               dpLi.name = response.data.hits.hits[i].fields.name[0];
               dpLi.id = response.data.hits.hits[i]._id;
               dpLi.description = response.data.hits.hits[i].fields.description[0];
               $scope.dpresultList.push(dpLi);
           }
      //  console.log("dpLi:"+ $scope.dpresultList.length); 
        }, function myError(response) {
            console.log("error:"+response.statusText); 
            $scope.dpresultList=[];
        });  
        //console.log("dp-query:"+queryString); 
}; 
$scope.getBluePrint = function(){
       console.log("get Blueprints function");
        var queryString ="http://ch3lxcmwasd01:9200/kb/bp/_search?q=";
        var filtersObj = [];
      //  console.log("filtersActive:"+JSON.stringify($scope.filtersActive));
        for(var i =0 ; i<$scope.filtersActive.length ;i++)
        {
            if($scope.filtersActive[i].id === "ioa_use_case"){
                filtersObj.push($scope.filtersActive[i]);
            }
        }
        queryString = queryString + "(";
        for(var j=0; j< filtersObj[0].options.length; j++)
         {
             if(j === 0)
                queryString = queryString +filtersObj[0].id+":\""+ filtersObj[0].options[j].replace("&","%26")+"\"";
              else
                queryString = queryString +" OR "+filtersObj[0].id+":\""+ filtersObj[0].options[j].replace("&","%26")+"\"";
           // console.log("obj:"+filtersObj[0].options[j]) ;
         }
        queryString = queryString+")";  
        //console.log("Get Blueprint:"+queryString);
        $http({
        method : "GET",
        url : queryString+"&size=9999"
        }).then(function mySucces(response) {
         //console.log("response:"+JSON.stringify(response.data.hits.hits) );
         $scope.bluePrints = response.data.hits.hits;
         $scope.bluePrints
         for(var i=0 ; i < $scope.bluePrints.length ; i++)
         {
             if($scope.bluePrints[i]._id === '1' )
                 $scope.bluePrints[i]._source.shortD = "Workplace Challenges";
             if($scope.bluePrints[i]._id === '2' )
                 $scope.bluePrints[i]._source.shortD = "Moving to the Edge";
             if($scope.bluePrints[i]._id === '3' )
                 $scope.bluePrints[i]._source.shortD = "Hybrid & Multi-Cloud Integration";
             if($scope.bluePrints[i]._id === '4' )
                 $scope.bluePrints[i]._source.shortD = " Real-time Analytics at the Edge";
         }
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        });  
}; 
$scope.arResultsList = [];
$scope.keyWordResult = [];
$scope.testFunction = function(){
    $state.go('dashboard.searchResults');
    $scope.keyWordResult = [];
    var query = "http://ch3lxcmwasd01.corp.equinix.com:9200/kb/_search?fields=_id,business_summary,customer,summary,document_name,name,description&q=('"+$scope.searchInput+"')&size=20";
    console.log("entering test function Query:"+query);
    $http({
        method : "GET",
        url : query
        }).then(function mySucces(response) {
      var localList = [];
        for(var i=0 ; i< response.data.hits.hits.length ; i++)
        {
            function resultObj(id,type,description,title,icon){
                this.id = id;
                this.type = type;
                this.description = description;
                this.title = title;
                this.icon = icon;
            }
            
            console.log("type:" + response.data.hits.hits[i]._type);
            if(response.data.hits.hits[i]._type === 'dp')
            {
                localList.push(new resultObj(response.data.hits.hits[i]._id, response.data.hits.hits[i]._type,response.data.hits.hits[i].fields.description[0],response.data.hits.hits[i].fields.name[0],"EQ-Asset-Infographic"));
            }
            else if(response.data.hits.hits[i]._type === 'ss')
            {
               localList.push( new resultObj(response.data.hits.hits[i]._id, response.data.hits.hits[i]._type,response.data.hits.hits[i].fields.business_summary[0],response.data.hits.hits[i].fields.customer[0],"EQ-Asset-Casestudy"));}
            else if(response.data.hits.hits[i]._type === 'ar')
            {
                localList.push( new resultObj(response.data.hits.hits[i]._id, response.data.hits.hits[i]._type,response.data.hits.hits[i].fields.summary[0],response.data.hits.hits[i].fields.document_name[0],"EQ-Asset-Survey"));}
            else
                console.log("this is blue print");
        }    
        $scope.keyWordResult = localList;
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        }); 
       $scope.searchFocus = false;
       jQuery("#csSearch").blur();
};
$scope.sugtClick = function(objKey){
    $scope.searchInput = objKey;
    $scope.testFunction();
};
$scope.getAr = function(){
    // console.log("getDBs function");
        var queryString ="http://ch3lxcmwasd01.corp.equinix.com:9200/kb/ar/_search?fields=_id,document_name,boxview_url,summary&q=";
        var filtersObj = [];
      //  console.log("filtersActive:"+JSON.stringify($scope.filtersActive));
        for(var i =0 ; i<$scope.filtersActive.length ;i++)
        {
            if($scope.filtersActive[i].id === "ioa_use_case"){
                filtersObj.push($scope.filtersActive[i]);
            }
        }
       // console.log("PLCD_Active:"+JSON.stringify(filtersObj));
            queryString = queryString + "(";
            for(var j=0; j< filtersObj[0].options.length; j++)
             {
                 if(j === 0)
                 {
                    queryString = queryString +"ioa_use_case\\:_"+ filtersObj[0].options[j].toLowerCase().replace("&","%26")+"_-_yes_or_no:\"Yes\"";
                 }
                  else
                    queryString = queryString +" OR "+"ioa_use_case\\:_"+ filtersObj[0].options[j].toLowerCase().replace("&","%26")+"_-_yes_or_no:\"Yes\"";
               // console.log("obj:"+filtersObj[0].options[j]) ;
             }
             queryString = queryString+")";
            if(i < filtersObj.length-1 )
            {
                queryString = queryString +" AND ";
            }      
        $http({
        method : "GET",
        //url : "http://localhost:9200/companies/company/_search?fields=_id,customer,customer_description&q=industry_vertical:(%22Manufacturing%22%20OR%20%22Health%20Care%22)%20AND%20primary_plcd:%22Locations%22"
        url : queryString+"&size=9999"
        }).then(function mySucces(response) {
           //console.log("response:"+ JSON.stringify(response.data));
           $scope.arResultsList = [];
           for(var i=0; i<response.data.hits.hits.length; i++){
             var ar = {
                "_id":"",
                "name":"",
                "url":"",
                "description":""
              };
             ar._id = response.data.hits.hits[i]._id;
             ar.name= response.data.hits.hits[i].fields.document_name[0] ;
             ar.url= response.data.hits.hits[i].fields.boxview_url[0] ;
             ar.description= response.data.hits.hits[i].fields.summary[0].substring(0,200)+"...." ;
             $scope.arResultsList.push(ar);
           
           }
      //  console.log("dpLi:"+ $scope.dpresultList.length); 
        }, function myError(response) {
            $scope.arResultsList = [];
            console.log("error:"+response.statusText); 
        });  
        //console.log("dp-query:"+queryString); 
};
$scope.search = function(){
    window.open("index.html#/Dashboard/tileResults", "_self");
    $scope.getdbs();
    $scope.getSuccessStories();
    $scope.getAr();
    $scope.getBluePrint();
};
$scope.openLink = function(urlIn){
    window.open(urlIn,"_blank");
};
    $scope.customerIn = "No response";
    $scope.getCustomeInfo = function(Id){
        console.log("retrive the customer details:"+$scope.elasticURL+"/_search?q=_id:"+Id);
        $http({
        method : "GET",
        url : $scope.elasticURL+"/_search?q=_id:"+Id
        }).then(function mySucces(response) {
           console.log("response:"+response.data);
           $scope.detailsObject = response.data.hits.hits[0]._source;
           $scope.customerIn = $scope.detailsObject["customer"];
           var values =[];
           for(var k in $scope.detailsObject) 
           {  
               if($scope.detailsObject[k]!== '')
                {
                   // console.log("Empty key:"+k);
                    var temp = [k.replace(/_/g, " "), $scope.detailsObject[k] ];
                    values.push(temp);
                }
           }
           $scope.detailsObject = values.sort();
           
          // $scope.test = values;
           console.log("Keys:"+values[0]);
        }, function myError(response) {
            console.log("error:"+response.statusText); 
        });  
        
    };
    $scope.checkFiltersOn = function(keyIn,valueIn){
      //console.log("KeyIn:"+keyIn+"-valueIn:"+valueIn);
      for (var i = 0; i < $scope.filtersActive[keyIn].options.length; i++) {
            if ($scope.filtersActive[keyIn].options[i] === valueIn) {
              //  console.log("true:checkFiletrsOn");
                return i;
            }
       }
      return -1;
    };
    $scope.activateFiletr = function(categoryKeyIn,filterKeyIn){
       // console.log("categoryKeyIn, ");
       var validator =  $scope.checkFiltersOn(categoryKeyIn,$scope.sideMenuObjs[categoryKeyIn].options[filterKeyIn]);
       if(validator>=0)
         {
           //  console.log("true:activateFilter:"+validator); 
             $scope.filtersActive[categoryKeyIn].options.splice(validator,1);
         }
       else
         {
            // console.log("false:activateFilter");
             $scope.filtersActive[categoryKeyIn].options.push($scope.sideMenuObjs[categoryKeyIn].options[filterKeyIn]);
         }
       //console.log("categoryKeyIN:"+categoryKeyIn+"filterKeyIn:"+filterKeyIn);
    };
    
     $scope.responseFlag = {
            "type":"",
            "message":"",
            "class":"",
            "hidden":true
        };
        $scope.progressBar = {
           "prgressBarClass":"progress-bar progress-bar-info progress-bar-striped active",
           "hidden":true
        };
        console.log("Hidden:"+ $scope.progressBar.hidden);
      $scope.uploadFiles = function(file, errFiles) {
//        $http({
//        method : "POST",
//        url : "http://10.195.114.69:8090/upload?filePath="
//        }).then(function mySucces(response) {
//           console.log("response:"+response.data);
//        }, function myError(response) {
//            console.log("error:"+response.statusText); 
//        }); 
//        
//        
//        
        $scope.progressBar.hidden = false;   
        $scope.f = file;
        $scope.errFile = errFiles && errFiles[0];
        if (file) {
            console.log("FIle:"+file);
            file.upload = Upload.upload({
                url: 'http://10.195.114.69:8090/upload',
                data: {filePath: file}
            });

            file.upload.then(function (response) {
                console.log("response:"+response.data);
                if(response.data.indexOf("Error") > -1)
                {    
                $scope.responseFlag.type = "Error:";
                $scope.responseFlag.message = response.data ; 
                $scope.responseFlag.class = "alert alert-danger fade in" ; 
                $scope.responseFlag.hidden = false;
                $scope.progressBar.prgressBarClass = "progress-bar progress-bar-danger progress-bar-striped active";
                }
                else
                {
                  $scope.responseFlag.type = "Sucess:";
                  if(response.data.indexOf("200")>-1)
                      $scope.responseFlag.message = "File uploaded successfully";
                  //$scope.responseFlag.message = response.data ; 
                  $scope.responseFlag.class = "alert-success fade in" ; 
                  $scope.responseFlag.hidden = false;
                  $scope.progressBar.hidden = false;
                  $scope.progressBar.prgressBarClass = "progress-bar progress-bar-success progress-bar-striped active";  
                }
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
               console.log("Error responnse:"+response.status);
                $scope.responseFlag.type = "Error:";
                if(response.status)
                    $scope.responseFlag.message = "Connection Refused: backend connection is refused" ; 
                $scope.responseFlag.class = "alert alert-danger fade in" ; 
                $scope.responseFlag.hidden = false;
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * 
                                         evt.loaded / evt.total));
            });
        }   
    };
 $(document).ready(function(){
     $.getJSON('data/sideMenuCategories.json', function(data) {
      $scope.sideMenuObjs= data;    
      $scope.getFileter();
      $scope.selectAll = function(){
          console.log("activating filters:"+ $scope.sideMenuObjs.length);
            for(var i=0 ; i<$scope.sideMenuObjs.length ; i++)
            {
                for(var j=0; j<$scope.sideMenuObjs[i].options.length; j++)
                {
                    $scope.activateFiletr(i,j);
                   // console.log("inner active");
                }
            }
      }; 
      $scope.selectAllSub = function(i){
         // console.log("selected:"+i);
         // console.log("selectALL:"+ JSON.stringify($scope.sideMenuObjs));
           for(var j=0; j<$scope.sideMenuObjs[i].options.length; j++)
                {
                    $scope.activateFiletr(i,j);
                   // console.log("inner active");
                }
        //   console.log("ActiveFileters:"+JSON.stringify( $scope.filtersActive));
      };
      setTimeout(function(){ 
           $scope.getdbs();
    $scope.getSuccessStories();
    $scope.getAr();
    $scope.getBluePrint(); }, 500);
  });
 });
  
}])
;

})();