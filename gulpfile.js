/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var gulp = require('gulp');

//sass plugin
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var cssnano = require('gulp-cssnano');
var ngAnnotate = require('gulp-ng-annotate');
var imagemin = require('gulp-imagemin');
gulp.task('default', function () {
    console.log('Running into the default task');
});
gulp.task('test', function(){
    console.log('Testing task is running now');
});

gulp.task('sass',function(){
    return gulp.src('public_html/source/scss/style.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(gulp.dest('public_html/source/css'));
    console.log('Sass to css transfer complete');
});
gulp.task('ang-anno', function () {
    return gulp.src('public_html/source/js/app.js')
        .pipe(ngAnnotate())
        .pipe(gulp.dest('public_html/source/js/'));
});
gulp.task('img-min', function(){
  return gulp.src('public_html/source/images/**/*.+(png|jpg|gif|svg)')
  .pipe(imagemin())
  .pipe(gulp.dest('public_html/dest/images'));
});
gulp.task('useref',['sass','ang-anno','img-min'], function(){
  return gulp.src('public_html/source/index.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('public_html/dest'));
});
gulp.task('watch', function(){
    gulp.watch('public_html/source/scss/**/*.scss',['sass']);
   // gulp.watch('public_html/source/js/**/*.js',['useref']);
    //gulp.watch('public_html/source/css/**/*.css',['useref-css']);
   // gulp.watch('public_html/source/images/**',['img-min']);
});